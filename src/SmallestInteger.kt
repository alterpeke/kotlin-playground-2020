/*This is a demo task.

Write a function:

fun solution(A: IntArray): Int

that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

Given A = [1, 2, 3], the function should return 4.

Given A = [−1, −3], the function should return 1.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [−1,000,000..1,000,000].*/

fun solution(): Int {

    val A = intArrayOf(-1, -3, -6, -4, -1, -2)

    A.sort()
    val matrix = A.distinct()

    val beginPoint = matrix.firstOrNull { it > 0 }

    beginPoint.let {
        if (it == null) {
            return 1
        } else {
            val zeroIndex = matrix.indexOf(it)
            var cont = it
            if (cont > 1) return 1
            for(i in zeroIndex until matrix.size){
                if (matrix[i] == cont){
                    cont++
                } else {
                    break
                }
            }
            return cont
        }
    }
}

fun main() {
    print(solution())
}