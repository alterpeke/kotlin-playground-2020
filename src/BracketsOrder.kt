import java.util.*

fun sayHello(name: String) {
    // You can print to STDOUT for debugging like you normally would:
    name.ifEmpty {
        println("Hello, there!")
        return
    }

    println("Hello, $name")
    return
}

fun validBraces(braces: String): Boolean {
    val stack = Stack<Char>()

    for (char in braces) {
        when (char) {
            '(', '[', '{' -> stack.push(char)
            ')', ']', '}' -> {
                if (stack.isEmpty() || !areMatchingBraces(stack.pop(), char)) {
                    return false
                }
            }
        }
    }

    return stack.isEmpty()
}

fun areMatchingBraces(opening: Char, closing: Char): Boolean {
    return when {
        opening == '(' && closing == ')' -> true
        opening == '[' && closing == ']' -> true
        opening == '{' && closing == '}' -> true
        else -> false
    }
}

fun main(){
    val input1 = "(){}[]"
    val input2 = "([{}])"
    val input3 = "(}"
    val input4 = "[(])"
    val input5 = "[({})](]"

    //println("Input 1 is valid: ${validBraces(input1)}") /* true */
    println("Input 2 is valid: ${validBraces(input2)}") // true
    println("Input 3 is valid: ${validBraces(input3)}") // false
    println("Input 4 is valid: ${validBraces(input4)}") // false
    println("Input 5 is valid: ${validBraces(input5)}") // false
}