/*your task is to implement the following function inside the University class:

fun getPaidCoursesWithTheNumbersOfSubscribedStudents(coursesCount: Int): Map<Course, Int>

this function should return a Map≤Course, Int> instance where the key is the Course type and it's value is the number of students enrolled to that course. The returned Map size should be equal to the number passed as the coursesCount: Int parameter. The returned map should be sorted descending by the number of the students subscribed to the course. The map instance should contain ONLY PAID courses

PRerequisites

There are the following predefined classes and objects available

Data Models

data class Student(val id: Id, val name: String, val subscribedCourses: List<Course>)

data class Course(val id: Id, val name: String, val isPaid: Boolean)

typealias Id = Int

Data Source:

interface Repository<T> {
    fun get(): Iterable<T>
}

And it's instance available as the property of the University class:

val repository : Repository<Student>

Requirements

There are the following requirements for the getPaidCoursesWithTheNumbersOfSubscribedStudetns() function:

1. The function should return a Map<Course, Int> instance, which entries correspond to the key value pairs of Course instances and the Int number of students subscribed to those courses.
2. Returned entries should be sorted descending by the number of subscribed students first of all (from the most students to the least)
3. The function should return only a number of entries limited by the value passed in the coursesCount: Int parameter. Please keep in mind that the limitation should happen on the entries aftier the are already sorted.
4. Courses with no subscribed students should not be included in the results
5. ONLY PAID courses hould be returned.

Hints

1. Call the get() function on the private val repository: Repository<Student> property to get the initial list of data. Then process it to fulfill the above requirements.
2. The performance od the solution will not be assessed, please focus only on its correctness.

Example

For example, when the Repository<Student>.get() function returns the following list of students:

1. Student(subscribedCourses = [Course(name = "Maths", isPaid = false), Course(name = "Arts", isPaid = true)])
2. Student(subscribedCourses = [Course(name = "History", isPaid = true), Course(name = "Biology", isPaid = true)])
3. Student(subscribedCourses = [Course(name = "Physics", isPaid = true), Course(name = "History", isPaid = true)])

Then the function getPaidCoursesWithTheNumbersOfSubscribedStudents(coursesCount: Int) invoked with the param coursesCount = 3 should return the following map entries:

1. Course(name = "History") to 2
2. Course(name = "Arts") to 1
3. Course(name = "Biology") to 1

Available packages/libraries

Kotlin 1.3.41
*/
data class Student(val id: Id, val name: String, val subscribedCourses: List<Course>)
data class Course(val id: Id, val name: String, val isPaid: Boolean)

typealias Id = Int

interface Repository<T> {
    fun get(): Iterable<T>
}

class University(private val repository: Repository<Student>) {

    fun getPaidCoursesWithTheNumbersOfSubscribedStudents(coursesCount: Int): Map<Course, Int> {
        val courseMap = mutableMapOf<Course, Int>()

        // Extracting all students from the repository
        val allStudents = repository.get()

        // Counting the number of subscribed students for each paid course
        for (student in allStudents) {
            for (course in student.subscribedCourses) {
                if (course.isPaid) {
                    courseMap[course] = courseMap.getOrDefault(course, 0) + 1
                }
            }
        }

        // Filtering out courses with no subscribed students
        val filteredCourseMap = courseMap.filter { it.value > 0 }

        // Sorting the courses based on the number of subscribed students
        val sortedCourseMap = filteredCourseMap.toList()
            .sortedByDescending { it.second }
            .toMap()

        // Limiting the number of courses based on coursesCount parameter
        val limitedCourseMap = sortedCourseMap.toList().take(coursesCount).toMap()

        return limitedCourseMap
    }
}

// Tests
class MockStudentRepository : Repository<Student> {
    private val students = listOf(
        Student(
            1, "John",
            listOf(Course(1, "Maths", false), Course(2, "Arts", true))
        ),
        Student(
            2, "Jane",
            listOf(Course(3, "History", true), Course(4, "Biology", true))
        ),
        Student(
            3, "Doe",
            listOf(Course(5, "Physics", true), Course(3, "History", true))
        )
    )

    override fun get(): Iterable<Student> {
        return students
    }
}

fun main() {
    val university = University(MockStudentRepository())
    val coursesCount = 3
    val result = university.getPaidCoursesWithTheNumbersOfSubscribedStudents(coursesCount)

    println("Result:")
    result.forEach { (course, count) ->
        println("Course: ${course.name}, Subscribed Students: $count")
    }
}
