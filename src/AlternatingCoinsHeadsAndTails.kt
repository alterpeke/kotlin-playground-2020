fun solution2(A: IntArray): Int {
    var flips = 0
    for (i in 1 until A.size) {
        if (A[i] == A[i - 1]) {
            flips++
            A[i] = 1 - A[i] // flip the coin
        }
    }
    return minOf(flips, A.size - flips)
}

fun main() {
    val A1 = intArrayOf(1, 0, 1, 0, 1, 1)
    val A2 = intArrayOf(1, 1, 0, 1, 1)
    val A3 = intArrayOf(0, 1, 0)
    val A4 = intArrayOf(0, 1, 1, 0)

    println(solution2(A1)) // 1
    println(solution2(A2)) // 2
    println(solution2(A3)) // 0
    println(solution2(A4)) // 2
}