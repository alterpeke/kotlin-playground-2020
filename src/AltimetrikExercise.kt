

fun orderNames(list: List<String>) : List<String> {
    val resultado = mutableListOf<String>()
    val repeatedNames = mutableListOf<String>()
    val repeatedSurnames = mutableListOf<String>()

    val orderedList = list.sortedWith(
        compareBy(
            { it.split(" ")[0] },
            { it.split(" ").getOrElse(1) { "" } })
    )

    for (fullName in orderedList){
        val split = fullName.split(" ")
        val name = split[0]
        val surname = split[1]
        var newName = name

        if (repeatedNames.contains(name)){
            newName = "$name ${surname.substring(0,1)}."
        }

        if (resultado.contains((name))){
            val prevName = repeatedNames.find {  it.split( " ")[0] == name }
            val indexPrevName = repeatedNames.indexOf(prevName)
            val prevSurname = repeatedSurnames[indexPrevName]

            val newRepName = "$name ${prevSurname.substring(0,1)}."

            resultado[indexPrevName] = newRepName
        }

        resultado.add(newName)
        repeatedSurnames.add(surname)
        repeatedNames.add(name)
    }
    return resultado
}

fun shortenNames(names: MutableList<String>): List<String> {
    // Step 1: Split names into components and prepare structures
    val nameParts = names.map { it.trim().split(" ") }
    val firstNameCounts = mutableMapOf<String, Int>()
    val resolvedNames = mutableListOf<String>()

    // Step 2: Count occurrences of each first name
    nameParts.forEach { parts ->
        val firstName = parts[0]
        firstNameCounts[firstName] = firstNameCounts.getOrDefault(firstName, 0) + 1
    }
    // Step 3: Resolve names
    names.forEach { fullName ->
        val parts = fullName.trim().split(" ")
        val firstName = parts[0]

        // If unique, use the first name
        if (firstNameCounts[firstName] == 1) {
            resolvedNames.add(firstName)
        } else {
            // Add last initial if available
            if (parts.size > 1) {
                val lastInitial = parts[1][0]
                val shortName = "$firstName $lastInitial."
                if (resolvedNames.contains(shortName)) {
                    // Use the full name if still ambiguous
                    resolvedNames.add(fullName)
                } else {
                    resolvedNames.add(shortName)
                }
            } else {
                // If no last name exists, fallback to full name
                resolvedNames.add(fullName)
            }
        }
    }

    // Step 4: Return resolved names in alphabetical order
    return resolvedNames.distinct().sorted()
}


fun main(){
    val namesInput = mutableListOf(
        "Alex Chow",
        "Brian Chesky",
        "Dan Fisher",
        "Dan Sugarman",
        "Diana Zuckerman",
        "Erin Bayshore",
        "Jaclyn Dagger",
        "James Lawrence",
        "Jesse Watson",
        "Justin Misson",
        "Lori Button",
        "Mike Linderman",
        "Morgan Armstrong",
        "Nathan Hillshore",
        "Patricia West",
        "Ted More",
        // 2nd level
        "Matthew Tim",
        "Erin Statenfield",
        "Dan Sticher",
        "Alan Pepper",
        "Rosie Spring",
        // 3rd level
        "Taylor Pips",
        "Danny Gilbert",
        "Erin Becker",
        "Mike Dew",
        "Dan Butter",
        "Dan Bayer",
        "Mike Lomo",
        "Jaclyn Donoghue",
        "Erin Singer",
        "Mike Laffen",
        // 4th level
        "Nathan Spring",
        "Kyle Van Esser",
        "Mary-Kate Sandra",
        "Matthew O'Connor",
        "Kyle Hurla",
        // 5th level
        "Alex",
        "Ted",
        // 6th level
        "Alex Chow"
    )

    println(shortenNames(namesInput))
}