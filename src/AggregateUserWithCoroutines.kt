/*You are required to implement a user data aggregator using coroutines by implementing an aggregateDataForCurrentUser function

Requirements

Your task is to resolve and aggregate user details in an aggregateDataForCurrentUser function.
you have APIs for resolving user details (UserEntity), user comments(List<CommentEntity>) and friends suggestions(List<FriendEntity>)

To resolve current user details, call the resolveCurrentUser function. To resolve user comments, call fetchUserComments with id resolved from UserEntity.id. To resolve user friends suggestions, call fetchSuggestedFriends with id resolved from UserEntity.id

Al of these functions are suspend functions; they need to be invoked in some coroutine context.

Implement functions in AggregateUserDataUseCase, where aggregateDataForCurrentUser must satisfy the following requirements

1. When resolveCurrentUser throws an error, forward the exception.
2. When resolveCurrentUser returns UserEntity, call fetchUserComments and fecthSuggestedFriends an the aggregate the results into AggregateData.
3. When fetchUserComments call takes longer than 2000ms, return an empty comments list.
4. When fetchUserComments throws an exception, return an empty comments list.
5. When fetchSuggestedFriends call takes longer than 2000ms, return an empty friends suggestion list.
6. When fetchSuggestedFriends throws an exception, return an empty friends suggestion list.

And for AggregateUserDataUseCase.close:
When close is invoked cancel all ongoing coroutine calls in provided scope.

Hints

Create additional CoroutineScope insde AggregateUserDataUseCase to handle cancellation when AggregateUserDataUseCase.close is invoked.

Classes accompanying AggregateUserDataUseCase

package coroutines

data class AggregatedData(
    val user: UserEntity,
    val comments: List<CommentEntity>,
    val suggestedFriends: List<FriendEntity>
)

typealias UserId = String

data class UserEntity(val id: UserId, val name: String)

data class CommentEntity(val id: String, val content: String)

data class FriendEntity(val id: String, val name: String)

interface AggregateUseCase{
    suspend fun aggregateDataForCurrentUser(): AggregatedData
}

Available packages/libraries

'org.jetbrains.kotlin:kotlin-stdlib:1.8.21'
'org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.6.4'*/

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.async
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.system.measureTimeMillis

// Classes accompanying AggregateUserDataUseCase
data class AggregatedData(
    val user: UserEntity,
    val comments: List<CommentEntity>,
    val suggestedFriends: List<FriendEntity>
)

typealias UserId = String

data class UserEntity(val id: UserId, val name: String)

data class CommentEntity(val id: String, val content: String)

data class FriendEntity(val id: String, val name: String)

interface AggregateUseCase {
    suspend fun aggregateDataForCurrentUser(): AggregatedData
}

class AggregateUserDataUseCase(
    private val resolveCurrentUser: suspend () -> UserEntity,
    private val fetchUserComments: suspend (String) -> List<CommentEntity>,
    private val fetchSuggestedFriends: suspend (String) -> List<FriendEntity>,
    private val coroutineScope: CoroutineScope = CoroutineScope(EmptyCoroutineContext)
) : AggregateUseCase {

    override suspend fun aggregateDataForCurrentUser(): AggregatedData {
        val user = coroutineScope.async { resolveCurrentUser.invoke() }.await()

        //Fetch User Comments
        val commentsDeferred = coroutineScope.async {
            try {
                withTimeoutOrNull(2000) {
                    fetchUserComments.invoke(user.id)
                } ?: emptyList()
            }catch (e: Exception){
                println("Exception occurred during fethUserComments: ${e.message}")
                emptyList<CommentEntity>()
            }
        }

        // Fetch Suggested Friends
        val suggestedFriendsDeferred = coroutineScope.async {
            try {
                withTimeoutOrNull(2000) {
                    fetchSuggestedFriends.invoke(user.id)
                } ?: emptyList()
            } catch (e: Exception) {
                println("Exception occurred during fetchSuggestedFriends: ${e.message}")
                emptyList<FriendEntity>()
            }
        }

        val comments = commentsDeferred.await()
        val suggestedFriends = suggestedFriendsDeferred.await()

        return AggregatedData(user, comments, suggestedFriends)
    }

    fun close() {
        coroutineScope.cancel()
    }
}

// Test cases
fun main() = runBlocking {
    val user = UserEntity("1", "John Doe")
    val comments = listOf(CommentEntity("1", "Great post"), CommentEntity("2", "Nice work"))
    val friends = listOf(FriendEntity("1", "Jane Doe"), FriendEntity("2", "Doe John"))

    val useCase = AggregateUserDataUseCase(
        resolveCurrentUser = { user },
        fetchUserComments = { userId ->
            delay(1000) // Simulating delay for fetching comments
            if (userId == user.id) comments else throw Exception("Failed to fetch comments")
        },
        fetchSuggestedFriends = { userId ->
            delay(1500) // Simulating delay for fetching suggested friends
            if (userId == user.id) friends else throw Exception("Failed to fetch suggested friends")
        }
    )

    try {
        val aggregatedData = useCase.aggregateDataForCurrentUser()
        println(aggregatedData)
    } catch (e: Exception) {
        println("Exception occurred: ${e.message}")
    }

    useCase.close()
}
