private fun isValidInput(input: String, regex: Regex): Boolean = regex.matches(input)

fun main() {
    //val input = "YourInputStringHere"  // IS VALID
    val input = "Hello, World!"          // IS NOT VALID
    //val regex = Regex("[a-zA-Z-'áéíóúÁÉÍÓÚ0-9ñÑ]+")
    val regex = Regex("[a-zA-Z-'áéíóúÁÉÍÓÚ0-9ñÑ ]+") // with space
    val result = if (isValidInput(input, regex)) "Input is valid" else "Input is not valid"
    println(result)
}