fun esPalindrome(palabra: String): Boolean{
    val indice = palabra.length-1
    for (i in 0..indice){
        if (palabra[i] != palabra[indice-i] && i<indice/2){
            return false
        }
    }
    return true
}

fun main(){
    val palabra = "reconocer"
    if (esPalindrome(palabra)) {
        println("Es palindrome")
    } else {
        println("No es palindrome")
    }
}