fun ordenarNombres(lista: List<String>): List<String> {
    val listaOrdenada = lista.sortedWith(compareBy({ it.split(" ")[0] }, { it.split(" ")[1] }))

    val resultado = mutableListOf<String>()
    val usados = mutableSetOf<String>()

    for (nombreCompleto in listaOrdenada) {
        val partes = nombreCompleto.split(" ")
        val nombre = partes[0]
        val apellido = partes[1]

        var nuevoNombre = nombreCompleto
        var indice = 0

        while (usados.contains(nuevoNombre)) {
            indice++
            nuevoNombre = "$nombre ${apellido.substring(0, minOf(indice, apellido.length))}"
        }

        // Añadimos al resultado y marcamos como usado
        resultado.add(nuevoNombre)
        usados.add(nuevoNombre)
    }

    return resultado
}

fun main() {
    val nombres = listOf(
        "Juan Perez",
        "Maria Lopez",
        "Juan Paredes",
        "Maria Luna",
        "Juan Perez",
        "Maria Lopez"
    )

    val resultado = ordenarNombres(nombres)
    println(resultado)
}