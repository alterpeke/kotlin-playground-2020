import kotlin.math.abs
import kotlin.math.max


fun maxTickets(tickets: Array<Int>): Int {
    var result: Int = 0

    tickets.sort()
    for(i in tickets.indices){
        for (j in tickets.indices){
            if ((tickets[j]-tickets[i]) <= 1){
                result = max(result, j-i+1)
            }
        }

    }

    return result
}




fun main() {
    val tickets = //arrayOf(8, 5, 4, 8,4)
                    arrayOf(4, 13, 2, 3)

    //val tickets = Array<Int>(ticketsCount, { 0 })
    //for (i in 0 until ticketsCount) {
    //    val ticketsItem = readLine()!!.trim().toInt()
    //    tickets[i] = ticketsItem
    //}

    val res = maxTickets(tickets)

    println(res)
}