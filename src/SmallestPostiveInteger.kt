fun solution(A: IntArray): Int {
    val set = HashSet<Int>()
    for (element in A) {
        set.add(element)
    }
    var result = 1
    while (set.contains(result)) {
        result++
    }
    return result
}

fun main() {
    val array1 = intArrayOf(1, 3, 6, 4, 1, 2)
    val array2 = intArrayOf(1, 2, 3)
    val array3 = intArrayOf(-1, -3)

    println(solution(array1)) // Output: 5
    println(solution(array2)) // Output: 4
    println(solution(array3)) // Output: 1
}