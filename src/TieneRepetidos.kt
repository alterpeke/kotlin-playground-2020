

fun tieneRepetidos(palabra: String): Boolean{
    var cont = 0
    for(j in 0 until palabra.length-1){
        for (i in 0 until palabra.length-1) {
            if (palabra[j] == palabra[i+1]){
                cont++
                if (cont>1) return true
            }
        }
    }
    return false
}


fun main(){
    val palabra = "cas"
    if (tieneRepetidos(palabra)) {
        print("Tiene repetidos")
    } else {
        print("No tiene repetidos")
    }
}