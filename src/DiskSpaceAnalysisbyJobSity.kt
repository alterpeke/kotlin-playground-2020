import java.util.ArrayDeque

/*
 * Complete the 'segment' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER x
 *  2. INTEGER_ARRAY space
 */

fun segment(x: Int, space: Array<Int>): Int {
    // Write your code here
    var bunchNum = 1
    var stack = ArrayDeque<Int>()
    stack.push(0)

    for (i in 1 until space.size) {
        // first bunch
        if (i < x) {
            if (space[i] < space[stack.peek()]) {
                stack.pop()
                stack.push(i)
            }
        } else {
            // rest of bunchs
            var peek = stack.peek()
            if (peek >= bunchNum) {
                if (space[i] < space[peek]) stack.push(i) else stack.push(peek)
            } else {
                stack.push(i)
                var j = bunchNum
                var count = 0
                while(count++ < x){
                    if (space[j] < space[stack.peek()]){
                        stack.pop()
                        stack.push(j)
                    }
                    j++
                }
            }
            bunchNum++
        }

    }
    return space[stack.max()!!]
}

fun divde(x: Int){
    if (x==0 || (3/x) == 3){
        print ("works")
    } else {
        print ("no work")
    }
}

fun main() {
    val array = intArrayOf(8,2,4,6)//intArrayOf(2, 5, 4, 6, 8)
    var x = 2
    print(segment(x, array.toTypedArray()))
    x = 0
    divde(x)
    print("13" + 5 + 3)
}