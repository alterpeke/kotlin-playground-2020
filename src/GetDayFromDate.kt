import java.text.SimpleDateFormat
import java.util.*

fun calculateDayName(dateString: String): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val date = dateFormat.parse(dateString)

    if (date != null) {
        val dayOfWeekFormat = SimpleDateFormat("EEEE", Locale.getDefault())
        return dayOfWeekFormat.format(date)
    }

    return ""
}

fun getFormatDate(dateString: String): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val date = dateFormat.parse(dateString)

    date?.let {
        return SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(it)
    }

    return ""
}

fun main(){
    //print(calculateDayName("2023-08-17"))
    print(getFormatDate("2023-08-17"))
}