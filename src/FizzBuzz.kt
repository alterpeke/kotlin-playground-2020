
/*
 * Complete the 'fizzBuzz' function below.
 *
 * The function accepts INTEGER n as parameter.
 */


fun fizzBuzz(n: Int) {
    for(k in 1 until n){
        // Write your code here
        if (k % 15 == 0) {
            println("FizzBuzz")

        } else if (k % 3 == 0) {
            println("Fizz")

        } else if (k % 5 == 0) {
            println("Buzz")

        } else {
            println(k.toString())
        }
    }

}




fun main(){
    val n = 12
    fizzBuzz(n)
}